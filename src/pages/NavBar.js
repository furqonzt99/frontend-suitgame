import React from "react";
import { Link } from "react-router-dom";
import "../css/NavBar.css";

const NavBar = () => {
  return (
    <div>
      <div className="bar">
        <ul className="ul">
          <div className="li">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/formregister">Regiter</Link>
            </li>
            <li>
              <Link to="/formlogin">Login</Link>
            </li>
            <li>
              <Link to="/formlistGame">Fights</Link>
            </li>
          </div>
        </ul>
      </div>
      <div className="h5">
        <h1>Welcome To Suit Game </h1>
      </div>
    </div>
  );
};

export default NavBar;
