import React, { useState } from "react";
import axios from "axios";
import Png from "../img/_.png";
import Batu from "../img/batu.png";
import Gunting from "../img/gunting.png";
import Kertas from "../img/kertas.png";
import Logo from "../img/logo 1.png";
import Refresh from "../img/refresh.png";
import "../css/FormListGame.css";

const FormListGame = () => {
  const [isError, setIsError] = useState(false);
  const [result, setResult] = useState(null);

  const submitHandler = (suit) => {
    const url = "https://suit-game-backend.herokuapp.com";
    const endpoint = `${url}/fights`;
    //console.log(endpoint); //buat ngedebug respoint dari backend
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    axios
      .post(
        endpoint,
        {
          suit,
        },
        config
      )
      .then((response) => {
        console.log(response);
        const { data } = response.data;
        const { result } = data;
        // console.log(result); debug
        setResult(result);
      })
      .catch(() => {
        setIsError(true);
      });
    // console.log(suit);
  };

  return (
    <section className="hal-1">
      <div className="logo-panah">
        <img src={Png} alt="_" />
      </div>
      <div className="logo">
        <img src={Logo} alt="logo" />
      </div>
      <div>
        <h5>ROCK PAPER SCISSORS</h5>
      </div>
      <div className="playerlogo">
        <p>PLAYER 1</p>
      </div>
      <div className="com">
        <p>com</p>
      </div>
      <div className="player">
        <div id="batu">
          <img onClick={() => submitHandler("R")} src={Batu} alt="batu" />
        </div>
        <div id="kertas">
          <img onClick={() => submitHandler("P")} src={Kertas} alt="kertas" />
        </div>
        <div id="gunting">
          <img onClick={() => submitHandler("S")} src={Gunting} alt="gunting" />
        </div>
      </div>
      <div>
        <div>
          <p id="kalkulasi" className="vs">
            {result ? result : "VS"}
          </p>
        </div>
      </div>
      <div className="com">
        <div className="player-com">
          <div className="batu2">
            <img src={Batu} alt="batu" />
          </div>
          <div className="kertas2">
            <img src={Kertas} alt="kertas" />
          </div>
          <div className="gunting2">
            <img src={Gunting} alt="gunting" />
          </div>
        </div>
      </div>
      <div id="refresh">
        <img
          src={Refresh}
          alt="refresh"
          onClick={() => {
            setResult("");
          }}
        />
      </div>
    </section>
  );
};

export default FormListGame;
