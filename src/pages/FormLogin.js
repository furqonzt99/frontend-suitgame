import React, { useState } from "react";
import axios from "axios";
import { Form, Row, Col, Button, Alert } from "react-bootstrap";
import "../css/FormLogin.css";

const FormLogin = () => {
  const [inputData, setInputData] = useState({
    username: "",
    password: "",
  });

  const [isError, setIsError] = useState(false);
  const [errMessage, setErrMessage] = useState("");

  const inputhandler = (e) => {
    // console.log(e.target.value); buat debug console onChebge()
    setInputData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));

    // console.log(inputData);  buat debug input data di console
  };
  const submitHandler = () => {
    // console.log(inputData);
    //request api Login
    const ENDPOINT = "https://suit-game-backend.herokuapp.com/";

    // const config = {
    //   headers: {
    //     Authorization: "Bearer " + localStorage.setItem("token"),
    //   },
    // };

    axios
      .post(
        `${ENDPOINT}login`,
        {
          username: inputData.username,
          password: inputData.password,
        }
        // config
      )
      .then((response) => {
        const { data, code } = response.data;

        // console.log(data);
        // localStorage.setItem();

        if (code === 200) {
          localStorage.setItem("token", data);
          window.location.href = "/formlistGame";

          // Router.redirec("fight room");
        }
      })
      .catch((err) => {
        // const { data:{message}} = err.response;
        const message = err.response.message;
        setIsError(true);
        // console.log(err.response); ngambil respon dari console browser
        setErrMessage(message);
      });
  };

  return (
    <div>
      {isError ? <Alert variant="danger">{errMessage}</Alert> : ""}
      <h5 className="form-h5"> Form Login </h5>
      <div className="Container">
        <div className="Container-Form">
          <Form>
            <div className="form-LOgin">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>User Name</Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  value={inputData.username}
                  onChange={inputhandler}
                  placeholder="User Name"
                />
              </Form.Group>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="3">
                  Password
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="password"
                    name="password"
                    value={inputData.password}
                    onChange={inputhandler}
                    placeholder="Password"
                  />
                </Col>
              </Form.Group>
              <Button variant="primary" onClick={submitHandler}>
                Submit
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default FormLogin;
