import React, { useState } from "react";
import axios from "axios";
import { Form, Row, Col, Button, Alert } from "react-bootstrap";
import "../css/FormRegister.css";

const FormRegister = () => {
  const [inputData, setInputData] = useState({
    username: "",
    name: "",
    email: "",
    password: "",
    bornDate: "",
  });

  const [isError, setIsError] = useState(false);

  const inputhandler = (e) => {
    setInputData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const submitHandler = () => {
    //request api register
    const ENDPOINT = "https://suit-game-backend.herokuapp.com/register";
    axios
      .post(ENDPOINT, {
        username: inputData.username,
        name: inputData.name,
        email: inputData.email,
        password: inputData.password,
        bornDate: inputData.bornDate,
      })
      .then((res) => {
        const { data } = res;
        console.log(data);
        window.location.href = "/formlogin";
      })
      .catch((err) => {
        setIsError(true);
      });
  };

  return (
    <div>
      {isError ? <Alert variant="danger"> Gagal register</Alert> : ""}
      <h5 className="form-h5"> Form Register </h5>
      <div className="Container">
        <div className="Container-Form">
          <div className="form">
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>User Name</Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  value={inputData.username}
                  onChange={inputhandler}
                  placeholder="User Name"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={inputData.name}
                  onChange={inputhandler}
                  placeholder=" Name"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  value={inputData.email}
                  onChange={inputhandler}
                  placeholder="name@example.com"
                />
              </Form.Group>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label column sm="3">
                  Password
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="password"
                    name="password"
                    value={inputData.password}
                    onChange={inputhandler}
                    placeholder="Password"
                  />
                </Col>
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Born Date</Form.Label>
                <Form.Control
                  type="date"
                  name="bornDate"
                  value={inputData.bornDate}
                  onChange={inputhandler}
                  placeholder="1999-06-15"
                />
              </Form.Group>
              <Button variant="primary" onClick={submitHandler}>
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormRegister;
