import React from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import FormRegister from "./pages/FormRegister";
import FormLogin from "./pages/FormLogin";
import FormListGame from "./pages/FormListGame";
import NavBar from "./pages/NavBar";
import "./css/app.css";
import "./css/NavBar.css";
function App() {
  return (
    <div className="nav">
      <div className="app">
        <Routes>
          <Route path="/" exact element={<NavBar />} />
          <Route path="/formregister" element={<FormRegister />} />
          <Route path="/formlogin" element={<FormLogin />} />
          <Route path="/formlistGame" element={<FormListGame />} />
        </Routes>
      </div>
    </div>
  );
}
export default App;
